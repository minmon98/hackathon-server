package main;

import static spark.Spark.get;
import static spark.Spark.post;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import com.google.gson.Gson;

import fptai.chatbot.sdk.manage.BotConstant;
import fptai.chatbot.sdk.manage.BotManager;
import fptai.chatbot.sdk.models.BotResponse;
import fptai.chatbot.sdk.models.Message;

/**
 * Hello world!
 *
 */
public class App {
	private static void sendPost(List<String> messages, String userId) throws Exception {

		JSONObject json = new JSONObject();
		json.put("user_id", userId);
		json.put("messages", messages.toString());

		HttpPost post = new HttpPost("https://hackathon-omi.herokuapp.com/webhook");

        // add request parameter, form parameters
        List<NameValuePair> urlParameters = new ArrayList<>();
        urlParameters.add(new BasicNameValuePair("user_id", userId));
        urlParameters.add(new BasicNameValuePair("messages", messages.toString()));
        
        post.setEntity(new UrlEncodedFormEntity(urlParameters, "UTF-8"));

        try (CloseableHttpClient httpClient = HttpClients.createDefault();
             CloseableHttpResponse response = httpClient.execute(post)) {

            System.out.println(EntityUtils.toString(response.getEntity()));
        }
	}

	public static void main(String[] args) {

		post("/receive-response", (request, response) -> {
			BotManager bmn = new BotManager();

			// Nội dung phản hổi từ bot nhận được qua cài đặt webhooks trên chat bot
			String bot_response = request.body();

			// Chuyển đổi nội dung phản hồi sang dạng đối tượng
			BotResponse bot_res_obj = bmn.parseResponse(bot_response);

			// Danh sách tin nhắn phản hồi từ bot
			List<Message> messages = bot_res_obj.getMessages();

			// Danh sách tin nhắn thô từ bot
			List<String> messagesRaw = new ArrayList();

			// Nhận message phản hồi
			for (Message message : messages) {
				String message_type = message.getType();

				switch (message_type) {
				case BotConstant.TEXT_MESSAGE:
					// Text Message Object
					System.out.println(message.getContentAsTextMessage().getText());
					messagesRaw.add(message.getContentAsTextMessage().getText());
					break;
				case BotConstant.IMAGE_MESSAGE:
					// Image Message Object
					System.out.println(message.getContentAsImageMessage().getUrl());
					messagesRaw.add(message.getContentAsImageMessage().getUrl());
					break;
				case BotConstant.CAROUSEL_MESSAGE:
					// Carousel Message Object
					System.out.println(message.getContentAsCarouselMessage().getCarouselCards());
					messagesRaw.add(message.getContentAsCarouselMessage().getCarouselCards().toString());
					break;
				case BotConstant.QUICK_REPLY_MESSAGE:
					// Quick reply Message Object
					System.out.println(message.getContentAsQuickReplyMessage().getText());
					messagesRaw.add(message.getContentAsQuickReplyMessage().getText());
					break;

				default:
					break;
				}
			}
			sendPost(messagesRaw, bot_res_obj.getSender_id());
			return "";
		});

		// Kiểm tra kết nối của chat bot với webhook khi cấu hình
		get("/receive-response", (request, response) -> {
			return "0df2e72e-cef8-11e9-bb65-2a2ae2dbcce4";
		});
	}
}